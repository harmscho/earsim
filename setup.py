from setuptools import find_packages, setup

setup(
    name='earsim',
    packages=find_packages('earsim'),
    version='0.0.0',
    description='extensive air-shower radio simulation library',
    author='Harm Schoorlemmer',
    license='MIT')
