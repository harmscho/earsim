from earsim import REvent
import numpy as np
import matplotlib.pyplot as plt
# file='/Users/harmscho/Analysis/ZHAireS/sample/Stshp_Iron_1.26_71.6_0.0_10.hdf5'
file='/Users/harmscho/Analysis/ZHAireS/sample/Stshp_Proton_3.98_87.1_90.0_24.hdf5'
plt.rcParams.update({'font.size': 16})

def get_polmax_sc(ev):
    rv  = []
    for a in ev.antennas:
        Eabs = a.Ex**2 + a.Ey**2 + a.Ez**2
        imax = np.argmax(Eabs)
        pol_max = np.array([a.Ex[imax],a.Ey[imax],a.Ez[imax]])
        rv_ = [np.dot(pol_max,ev.uAxB),np.dot(pol_max,ev.uAxAxB),np.dot(pol_max,ev.uA)]
        rv.append(rv_)
    return rv

ev = REvent(file)
x = np.asanyarray([a.x for a in ev.antennas])
y = np.asanyarray([a.y for a in ev.antennas])

pol = get_polmax_sc(ev)
U = []
V = []
for p in pol:
    U.append(p[0])
    V.append(p[1])
U = np.asarray(U)
V = np.asarray(V)
normUV = (U**2 + V**2)**0.5
x_ = []
y_ = []
for _x,_y in zip(x,y):
    x_.append(np.dot([_x,_y,0],ev.uAxB))
    y_.append(np.dot([_x,_y,0],ev.uAxAxB))
x_ = np.asarray(x_)
y_ = np.asarray(y_)

fig = plt.figure(figsize=(10,8))
plt.quiver(x_/1e3,y_/1e3,U/normUV,V/normUV,np.log10(normUV), pivot='mid',cmap='Spectral_r')
prange =3
plt.xlim(-prange,prange)
plt.ylim(-prange,prange)
plt.grid()
cb = plt.colorbar()
fig.show()
#

fig = plt.figure(figsize=(10,8))
plt.scatter(x_/1e3,y_/1e3,s =100,c=np.rad2deg(np.arctan2(V,U)),cmap='RdBu')
plt.grid()
cb = plt.colorbar()
cb.ax.set_title(r'$\Delta (^\circ)$')
plt.ylim(-prange,prange)
plt.xlim(-prange,prange)

plt.xlabel(r'$-\vec{v}\times \vec{B} (km)$')
plt.ylabel(r'$\vec{v}\times\vec{v}\times \vec{B}$ (km)')
fig.show()
