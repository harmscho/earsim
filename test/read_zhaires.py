#%%
from earsim import REvent
from earsim import *
import numpy as np
import matplotlib.pyplot as plt
file='/Users/harmscho/Analysis/ZHAireS/sample/Stshp_Iron_1.26_71.6_0.0_10.hdf5'
ev = REvent(file)
#get coordinates
x = np.asanyarray([a.x for a in ev.antennas])
y = np.asanyarray([a.y for a in ev.antennas])
maxVals = [np.max([np.max(a.Ex),np.max(a.Ey),np.max(a.Ez)])for a in ev.antennas]
lmaxVals=np.asanyarray( np.log10(maxVals))
im = np.argmax(maxVals)
print(im)

#%%

fig = plt.figure()
plt.scatter(x,y,c=lmaxVals,cmap="Spectral_r")
plt.colorbar()

#%%
fig, axs = plt.subplots(2,1,figsize=(10,10))
# plt.plot(ev.antennas[i].t,ev.antennas[i].Ex,alpha=0.3)
for i in range(im+5,im+15):
    E =ev.antennas[i].Ey
    t= ev.antennas[i].t-ev.antennas[i].t[0]
    E=E[::4]
    t=t[::4]

    dt=t[1]
    axs[0].plot(t,E)
    axs[0]
    axs[0].set_xlim(190,210)
    fr,de = get_group_delay(E,dt)
    axs[1].plot(fr[1:],de[1:])
    axs[1].set_xscale('log')
    axs[1].set_ylim(190,215)
dt
plt.plot(fr,de)
#%%

# plt.plot(ev.antennas[i].t,ev.antennas[i].Ez,alpha=0.3)
E = ev.antennas[i]

fig.show()

file='/Users/harmscho/Analysis/rit/shower_development/ARENA/example_shower/Event55.sry'
ev = REvent(file)
#get coordinates
ev.zenith
x = np.asanyarray([a.x for a in ev.antennas])
y = np.asanyarray([a.y for a in ev.antennas])
maxVals = [np.max([np.max(a.Ex),np.max(a.Ey),np.max(a.Ez)])for a in ev.antennas]
lmaxVals=np.asanyarray( np.log10(maxVals))

fig = plt.figure()
plt.scatter(x,y,c=lmaxVals,cmap="Spectral_r")
plt.colorbar()
fig.show()

fig = plt.figure()
plt.plot(ev.antennas[0].t,ev.antennas[0].Ex,alpha=0.3)
plt.plot(ev.antennas[0].t,ev.antennas[0].Ey,alpha=0.3)
plt.plot(ev.antennas[0].t,ev.antennas[0].Ez,alpha=0.3)
fig.show()
