from numpy.random import uniform
import numpy as np
import os
import matplotlib.pyplot as plt
from pathlib import Path

def rectangular_grid(spacingx=50,array_sidex=3000):
    x = np.linspace(-spacingx/2, spacingx/2, round(array_sidex/spacingx))
    y = np.linspace(-array_side/2, array_side/2, round(array_side/spacing))
    print(x)
    xv, yv = np.meshgrid(x, y)
    return xv,yv

def square_grid(spacing=10,array_side=1000):
    x = np.linspace(-array_side/2, array_side/2, round(array_side/spacing))
    y = np.linspace(-array_side/2, array_side/2, round(array_side/spacing))
    print(x)
    xv, yv = np.meshgrid(x, y)
    return xv,yv

def hex_grid(spacing=10,array_side=1000):
    d = spacing*np.sin(np.pi/3)
    ny = round(0.5*array_side/d)
    nx = round(0.5*array_side/d)
    x = []
    y = []
    #make hex grid for tanks
    for i in range(-ny,ny):
        hcoord = spacing*np.arange(-nx,nx,1) + (i%2)*spacing/2
        vcoord = np.full(len(hcoord),i*d)#*0.75)
        x.append(hcoord)
        y.append(vcoord)
    return x,y

def antenna_list(xx,yy,h=0,mode="coreas"):
    count = 0
    lines=[]
    for x_,y_ in zip(xx,yy):
        for x,y in zip(x_,y_):
            line = ""
            if "zhs":
                line = "AddAntenna ant%d %.1f %.1f %.1f"%(count,x,y,h)
            elif "coreas":
                line = "AntennaPosition = %.1f\t%.1f\t%.1f ant%d"%(x*100,y*100,h*100,count)
            lines.append(line)
            count += 1
    return lines

def template_zhaires(task="mysim",primary="proton",E=1,
                     seed=0.1,Bmag=54.021,Binc=57.45,Bdec=0.0,
                     ground=0,thin=1e-7,zen=20.,azi=90.,dt=0.5):
    lines = []
    lines.append("Task "+ task)
    lines.append("Primary "+primary)
    lines.append("PrimaryZenAngle %.2f deg"%(zen))
    lines.append("PrimaryAzimAngle %.2f deg"%(azi))
    lines.append("PrimaryEnergy %.2f PeV"%(E))
    lines.append("RandomSeed %.4f"%(seed))
    lines.append("ZHAireS On")
    lines.append("FresnelTime On")
    lines.append("GeomagneticField %.2f uT %.2f deg %.2f deg"%(Bmag,Binc,Bdec))
    lines.append("GroundAltitude %.2f m"%(ground))
    lines.append("Thinning %.2e Rel"%(thin))
    lines.append("ThinningWFactor 0.06")
    lines.append("TotalShowers 1")
    lines.append("AntennaTimeMin -10 ns")
    lines.append("AntennaTimeMax 490 ns")
    lines.append("TimeDomainBin %.2f ns"%(dt))
    lines.append("ElectronCutEnergy 3 MeV")
    lines.append("ElectronRoughCut 3 MeV")
    lines.append("GammaCutEnergy 3 MeV")
    lines.append("GammaRoughCut 3 MeV")
    lines.append("ForceLowEDecay Never")
    lines.append("ForceLowEAnnihilation Never")
    lines.append("ObservingLevels 510 100.000 km 2.794 km")
    lines.append("End")
    return lines

def template_corsika(seed = 60378,E=1e6,primary="proton",zen=0,azi=0,thin=1e-6,ground=0):
    particles = {
    "gamma": 1,
    "proton": 14,
    "helium": 402,
    "nitrogen": 1407,
    "iron":5626
    }

    lines = []
    lines.append("RUNNR	1")
    lines.append("EVTNR	1")
    lines.append("PARALLEL		+1.000000E+03	+5.000000E+05	+1	F")
    lines.append("SEED	60378	0	0")
    lines.append("SEED	"+ str(seed) + "	0	0")
    lines.append("SEED	"+ str(seed+1) + "	0	0")
    lines.append("SEED	"+ str(seed+2) + "	0	0")
    lines.append("SEED	"+ str(seed+3) + "	0	0")
    lines.append("SEED	"+ str(seed+4) + "	0	0")
    lines.append("PRMPAR	%d"%(particles[primary]))
    lines.append("ERANGE	%.4E %.4E"%(E,E))
    lines.append("THETAP	%.4E %.4E"%(zen,zen))
    lines.append("PHIP	    %.4E %.4E"%(azi,azi))
    lines.append("THIN	%.2E	+1.000000E+04	5.E+03"%(thin))
    lines.append("MAGNET	+1.948230E+01	-1.412410E+01")
    lines.append("DIRECT	./")
    lines.append("DATDIR	/vol/app/software/corsika-77410/run/")
    lines.append("STEPFC	1")
    lines.append("NSHOW	1")
    lines.append("ECUTS	3.0E-01 1.0E-02 2.5E-04 2.5E-04")
    lines.append("ELMFLG	T T")
    lines.append("1.0E+00 1.0E+02")
    lines.append("OBSLEV	%.4E  value in cm"%(ground*100))
    lines.append("ECTMAP	1.0E+05")
    lines.append("MUADDI	T")
    lines.append("MUMULT	T")
    lines.append("MAXPRT	1")
    lines.append("PAROUT	T F")
    lines.append("LONGI	T 5.0E+00 T T")
    lines.append("RADNKG	5.0E+05")
    lines.append("ATMOD	27")
    lines.append("DATBAS	F")
    lines.append("USER	Rocky")
    lines.append("EXIT")

# def template_coreas():
#     # CoREAS V1.4 by Tim Huege <tim.huege@kit.edu> with contributions by Marianne Ludwig and Clancy James - parameter file
#
# # parameters setting up the spatial observer configuration:
#
# CoreCoordinateNorth = 0			; in cm
# CoreCoordinateWest = 0			; in cm
# CoreCoordinateVertical = 139999.8			; in cm
#
# # parameters setting up the temporal observer configuration:
#
# TimeResolution = 2e-10				; in s
# AutomaticTimeBoundaries = 4e-07			; 0: off, x: automatic boundaries with width x in s
# TimeLowerBoundary = -1				; in s, only if AutomaticTimeBoundaries set to 0
# TimeUpperBoundary = 1				; in s, only if AutomaticTimeBoundaries set to 0
# ResolutionReductionScale = 0			; 0: off, x: decrease time resolution linearly every x cm in radius
#
# # parameters setting up the simulation functionality:
# GroundLevelRefractiveIndex = 1.000312		; specify refractive index at 0 m asl
#
# # event information for Offline simulations:
#
# EventNumber = 1
# RunNumber = 37201
# GPSSecs = 1052035339
# GPSNanoSecs = 400000000
# CoreEastingOffline = -1243.987				; in meters
# CoreNorthingOffline = 6236.675				; in meters
# CoreVerticalOffline = -3.1817				; in meters
# OfflineCoordinateSystem = Reference
# RotationAngleForMagfieldDeclination = 2.087		; in degrees
# Comment = Event 1:Use at 2013-05-08T08:02:03.399999999Z
#
# # event information for your convenience and backwards compatibility with other software, these values are not used as input parameters for the simulation:
#
# ShowerZenithAngle = 82.49999717			; in degrees
# ShowerAzimuthAngle = 180.000005		; in degrees, 0: shower propagates to north, 90: to west
# PrimaryParticleEnergy = 1e+19			; in eV
# PrimaryParticleType = 14			; as defined in CORSIKA
# DepthOfShowerMaximum = 6.8350e+02		; slant depth in g/cm^2
# DistanceOfShowerMaximum = -1		; geometrical distance of shower maximum from core in cm
# MagneticFieldStrength = 0.2406346191			; in Gauss
# MagneticFieldInclinationAngle = -35.94101765		; in degrees, >0: in northern hemisphere, <0: in southern hemisphere
# GeomagneticAngle = 151.5589852			; in degrees
# CorsikaFilePath = ./
# CorsikaParameterFile = RUN007801.inp

def write_zhaires(task="zhstest",al="",inp="",nsplit=0,path="./"):
    dir = path+task
    os.makedirs(path+task, exist_ok=True)

    if nsplit == 0 or nsplit >= len(al):
        print("if",nsplit,len(al))
        with open(dir + "/" + task + ".inp", 'w') as f:
            for l in inp[:-1]:
                f.write(l + "\n")
            for l in al:
                f.write(l + "\n")
            f.write(inp[-1] + "\n")
    else: #split
        print("else")
        ind = np.arange(0,len(al),nsplit)
        for sub,i in enumerate(ind):
            dir = path+task+ "/sub" +  str(sub)
            fname = dir + "/" + task + ".inp"
            print(fname)
            os.makedirs(dir, exist_ok=True)
            with open(fname, 'w') as f:
                for l in inp[:-1]:
                    f.write(l + "\n")
                for l in al[i:np.min([i+nsplit,len(al)])]:
                    f.write(l + "\n")
                f.write(inp[-1] + "\n")

# xs,ys= square_grid(15,500)
# plt.scatter(xs,ys,alpha=0.2)
# print(np.shape(xs))

xh,yh= hex_grid(25,600)
al = antenna_list(xh,yh,mode="zhs")
plt.scatter(xh,yh,alpha=0.2)
print(np.shape(xh),"= ",np.shape(xh)[0]*np.shape(xh)[1])

al = antenna_list(xh,yh,mode="zhs")
#high freq studies
parts = ['gamma','proton','NZ 2 2','NZ 7 7','iron']
folders = ['gamma','proton','helium','nitrogen','iron']
Nevt = 30
Es =[0.1,1,10,100]
Nant = [100,100,40,20]
for E,Na in zip(Es,Nant):
    for part,fold in zip(parts,folders):
        for i in range(0,Nevt):
            seed = uniform(0,1)
            inp = template_zhaires(seed=seed,thin=1e-6,dt=0.25,primary=part,E=E)
            # print("/Users/harmscho/Analysis/high_freq/sims/nikhef/E"+str(E)+"PeV/"+fold+"/")
            write_zhaires("event"+str(i),al,inp,Na,path="/Users/harmscho/Analysis/high_freq/sims/nikhef/E"+str(E)+"PeV/"+fold+"/")

xs,ys= square_grid(15,500)
#high zenith studies
parts = ['gamma','proton','iron']
folders = ['gamma','proton','iron']
Nevt = 1
Es =[10]
Nant = [10]

for E,Na in zip(Es,Nant):
    for part,fold in zip(parts,folders):
        for i in range(0,Nevt):
            seed = uniform(0,1)
            inp = template_zhaires(seed=seed,thin=1e-6,dt=0.25,primary=part,E=E)
            # print("/Users/harmscho/Analysis/high_freq/sims/nikhef/E"+str(E)+"PeV/"+fold+"/")
            write_zhaires("event"+str(i),al,inp,Na,path="/Users/harmscho/Analysis/high_freq/sims/nikhef/E"+str(E)+"PeV/"+fold+"/")



c = 0.01
print("%.2E"%(c))


particles = {
"gamma": 1,
"proton": 14,
"helium": 402,
"nitrogen": 1407,
"iron":5626
}
particles["iron"]
