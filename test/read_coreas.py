from earsim import *
import numpy as np
import matplotlib.pyplot as plt
file='../../Auger/simulation/radio/has_hybrid/SIM007801.reas'

ev = REvent(file)
ev.uA
ev.uAxB
ev.uAxAxB
x = np.asanyarray([a.x for a in ev.antennas])
y = np.asanyarray([a.y for a in ev.antennas])
maxVals = [np.max([np.max(a.Ex),np.max(a.Ey),np.max(a.Ez)])for a in ev.antennas]
lmaxVals=np.asanyarray( np.log10(maxVals))
cut = lmaxVals>-8

fig = plt.figure()
plt.scatter(x[cut],y[cut],c=lmaxVals[cut])
plt.colorbar()
fig.show()

fig = plt.figure()
ev.antennas[0].name
plt.plot(ev.antennas[0].t,ev.antennas[0].Ex,alpha=0.3)
plt.plot(ev.antennas[0].t,ev.antennas[0].Ey,alpha=0.3)
plt.plot(ev.antennas[0].t,ev.antennas[0].Ez,alpha=0.3)
fig.show()

dt = ev.antennas[0].t[1]-ev.antennas[0].t[0]
fig = plt.figure()
plt.plot(ev.antennas[0].t,block_filter(ev.antennas[0].Ex,dt,0.0,0.03),alpha=0.3)
plt.plot(ev.antennas[0].t,block_filter(ev.antennas[0].Ey,dt,0.0,0.03),alpha=0.3)
plt.plot(ev.antennas[0].t,block_filter(ev.antennas[0].Ez,dt,0.0,0.03),alpha=0.3)
fig.show()


v1 = np.array([1,0,1,2])
v2 = np.array([1,0,-1,2])
v = np.vstack((v1,v2))
np.dot(v,[0,0,1])

Exyz = ev.antennas[0].Ex
np.hstack((Exyz, ev.antennas[0].Ey))
