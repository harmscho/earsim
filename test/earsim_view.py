import tkinter as tk
from tkinter import ttk
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from earsim import REvent
from earsim import tools as et
import sys

plt.rcParams['axes.axisbelow'] = True
import sys

c_m_ns=299792458./1e9 #speed of light m/ns

fileNames = sys.argv[1:]
if len(fileNames) == 0:
    fileNames.append('/Users/harmscho/Auger/simulation/radio/has_hybrid/SIM007801.reas')

#general layout: tabs with canvas + control buttons + general options down
root = tk.Tk()
root.wm_title("Embedding in Tk")
tabControl = ttk.Notebook(root)
tab1 = ttk.Frame(tabControl)
tab2 = ttk.Frame(tabControl)
tabControl.add(tab1, text ='Array View')
tabControl.add(tab2, text ='Signals ')
tabControl.pack(expand = 1, fill ="both")
gframe = ttk.Frame(master=root,borderwidth=1)
gframe.pack(side=tk.TOP,fill=tk.BOTH, expand=1)
#make matplotlib canvasii
#figure 1
fig1 = Figure(figsize=(5, 4), dpi=100)
ax1 = fig1.add_subplot(111)
cframe1 = tk.Frame(master=tab1, borderwidth=10)
canvas1 = FigureCanvasTkAgg(fig1, master=cframe1)
canvas1.draw()
canvas1.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
toolbar = NavigationToolbar2Tk(canvas1, cframe1)
toolbar.update()
canvas1.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
cframe1.pack(side=tk.LEFT,fill=tk.BOTH, expand=1)
#figure 2
fig2 = Figure(figsize=(5, 4), dpi=100)
ax2 = fig2.add_subplot(111)
cframe2 = tk.Frame(master=tab2, borderwidth=10)
canvas2 = FigureCanvasTkAgg(fig2, master=cframe2)  # A tk.DrawingArea.
canvas2.draw()
canvas2.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
toolbar = NavigationToolbar2Tk(canvas2, cframe2)
toolbar.update()
canvas2.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
cframe2.pack(side=tk.LEFT,fill=tk.BOTH, expand=1)

#global variables needed by functions/widgets
currentEventIndex = 0
ev = None
tStep=0
freqMax=0
sval0 = tk.StringVar(root,'0.00') #f0 -- set in tab1 (maybe move to general)
sval1 = tk.StringVar(root,"%.2f"%(freqMax))#f2 -- set in tab1 (maybe move to general)
alabels = tk.StringVar(value='')
#used in tab1
ecomp = tk.StringVar(root,'Ex') #select efield component
plttype = tk.StringVar(root,'maxampl') #type of array plot
coord = tk.StringVar(root,'xyz') #coordinate system array plot
useLog= tk.IntVar(root,0) #log color scale
textLabel= tk.IntVar(root,0) #show station text labels
useAxB=tk.IntVar(root,0) #use AxB
cb = None
#used in tab2
stx = tk.IntVar(root,1) #component of Efield
sty = tk.IntVar(root,0) #component of Efield
stz = tk.IntVar(root,0) #component of Efield
ft = tk.StringVar(root,'t') #frequency or time domain
useLogx= tk.IntVar(root,0) #log x scale
useLogy= tk.IntVar(root,0) #log y scale

#FUNCTIONS
def update_event():
    global ev,tStep,freqMax,alabels,text_box
    fileNames
    print('reading ' + fileNames[currentEventIndex])
    try:
        ev = REvent(fileNames[currentEventIndex])
        print("event with %d"%(len(ev.antennas)))
        tStep = ev.antennas[0].t[1]-ev.antennas[0].t[0]
        freqMax = 0.5/tStep
        sval1.set("%.2f"%(freqMax))
        alabels.set(tuple([a.name for a in ev.antennas]))
        s = print_event_info()
        text_box.delete(0.0, 'end')
        text_box.insert('end',s)
    except:
        print('cannot read:',fileNames[currentEventIndex], 'trying next event')
        _next()

def array_plot(ax):
    global cb
    if cb != None:
        cb.remove()
    ax.clear()
    x = np.asanyarray([a.x for a in ev.antennas])
    y = np.asanyarray([a.y for a in ev.antennas])
    xlabel = ""
    ylabel = ""
    zlabel = ""

    if coord.get() == 'xyz':
        x_ = x
        y_ = y
        xlabel = "x (m)"
        ylabel = "y (m)"
    if coord.get() == 'auger':
        y_ = x
        x_ = -y
    if coord.get() == 'shower':
        u,v,w = et.get_uvw(ev.zenith,ev.azimuth)
        x_ = []
        y_ = []
        for _x,_y in zip(x,y):
            x_.append(np.dot([_x,_y,0],u))
            y_.append(np.dot([_x,_y,0],v))
        xlabel = "x' (m) - shower plane"
        ylabel = "y' (m) - shower plane"
    if coord.get() == '-vxB':
        x_ = []
        y_ = []
        for _x,_y in zip(x,y):
            x_.append(np.dot([_x,_y,0],ev.uAxB))
            y_.append(np.dot([_x,_y,0],ev.uAxAxB))
        xlabel = r'$-\vec{v}\times \vec{B} (m)$'
        ylabel = r'$\vec{v}\times\vec{v}\times \vec{B}$ (m)'

    vals = []
    labels = []
    for a in ev.antennas:
        E = None
        labels.append(a.name)
        if ecomp.get() == 'Ex':
            E = et.block_filter(a.Ex,tStep,float(sval0.get()),float(sval1.get()))
            zlabel=r'$E_{x}/(\mu Vm^{-1})$'
        if ecomp.get() == 'Ey':
            E = et.block_filter(a.Ey,tStep,float(sval0.get()),float(sval1.get()))
            zlabel=r'$E_{y}/(\mu V m^{-1})$'
        if ecomp.get() == 'Ez':
            E = et.block_filter(a.Ez,tStep,float(sval0.get()),float(sval1.get()))
            zlabel=r'$E_{z}/(\mu V m^{-1})$'
        if ecomp.get() == 'Eabs':
            Ex = et.block_filter(a.Ex,tStep,float(sval0.get()),float(sval1.get()))
            Ey = et.block_filter(a.Ey,tStep,float(sval0.get()),float(sval1.get()))
            Ez = et.block_filter(a.Ez,tStep,float(sval0.get()),float(sval1.get()))
            E = np.sqrt(Ex**2+Ey**2+Ez**2)
            zlabel=r'$|E|/(\mu V m^{-1})$'
        if plttype.get() == 'time':
            if coord.get() == 'xyz' or coord.get() == 'auger':
                vals.append(a.t[np.argmax(np.abs(E))])
            else:
                dt =  np.dot([a.x,a.y,0.],ev.uA)/c_m_ns
                vals.append(a.t[np.argmax(np.abs(E))]+dt)
            zlabel=r'$t_{max}/(ns)$'
        if plttype.get() == 'maxampl':
            vals.append(np.max(np.abs(E)))
        if plttype.get() == 'sumquad':
            zlabel = 'sum (' + zlabel + ')^2'
            vals.append(np.sum(E*E))
    vals = np.asarray(vals)
    if useLog.get() == 1 and plttype.get() != 'time':
        im = ax.scatter(x_,y_,c=vals,cmap='Spectral_r',norm=mpl.colors.LogNorm())
    elif plttype.get() == 'time':
        im = ax.scatter(x_,y_,c=vals,cmap='Spectral_r')
    else:
        #im = ax.scatter(x_,y_,c=vals,s=150*vals/np.max(vals),cmap='Spectral_r')
        im = ax.scatter(x_,y_,c=vals,cmap='Spectral_r')
    cb = fig1.colorbar(im, ax=ax)
    if textLabel.get() == 1:
        for _l,_x,_y in zip(labels,x_,y_):
            ax.annotate(_l, (_x,_y), textcoords="offset points", xytext=(0,10), ha='center')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    cb.ax.set_title(zlabel)
    ax.grid()

def _quit():
    root.quit()     # stops mainloop
    root.destroy()  # this is necessary on Windows to prevent
                    # Fatal Python Error: PyEval_RestoreThread: NULL tstate
def _update_array():
    global ax1
    array_plot(ax1)
    canvas1.draw()

def station_plot(ax):
    ax.clear()
    sel_ant = listbox.curselection()
    for sa in sel_ant:
        al = listbox.get(sa)
        for a in ev.antennas:
            if a.name == al:
                if ft.get() == 'f':
                    if useLogx.get() == 1:
                        ax.set_xscale('log')
                    if useLogy.get() == 1:
                        ax.set_yscale('log')
                if ft.get() == 'g':
                    if useLogx.get() == 1:
                        ax.set_xscale('log')
                if stx.get() == 1:
                    E = None
                    if useAxB.get() == 1:
                        E = [np.dot(ev.uAxB,[ex,ey,ez]) for ex,ey,ez in zip(a.Ex,a.Ey,a.Ez)]
                    else:
                        E = a.Ex
                    if ft.get() == 'f':
                        f,amp = et.get_freq_spec(E,tStep)
                        ax.plot(f,amp)
                    elif ft.get() == 'g':
                        f,amp = et.get_group_delay(E,tStep,20)
                        ax.plot(f,amp)
                    else:
                        E = et.block_filter(E,tStep,float(sval0.get()),float(sval1.get()))
                        ax.plot(a.t,E)
                if sty.get() == 1:
                    E = None
                    if useAxB.get() == 1:
                        E = [np.dot(ev.uAxAxB,[ex,ey,ez]) for ex,ey,ez in zip(a.Ex,a.Ey,a.Ez)]
                    else:
                        E = a.Ey
                    if ft.get() == 'f':
                        f,amp = et.get_freq_spec(E,tStep)
                        ax.plot(f,amp)
                    elif ft.get() == 'g':
                        f,amp = et.get_group_delay(E,tStep,20)
                        ax.plot(f,amp)
                    else:
                        E = et.block_filter(E,tStep,float(sval0.get()),float(sval1.get()))
                        ax.plot(a.t,E)
                if stz.get() == 1:
                    E = None
                    if useAxB.get() == 1:
                        E = [np.dot(ev.uA,[ex,ey,ez]) for ex,ey,ez in zip(a.Ex,a.Ey,a.Ez)]
                    else:
                        E = a.Ez
                    if ft.get() == 'f':
                        f,amp = et.get_freq_spec(E,tStep)
                        ax.plot(f,amp)
                    elif ft.get() == 'g':
                        f,amp = et.get_group_delay(E,tStep,20)
                        ax.plot(f,amp)
                    else:
                        E = et.block_filter(E,tStep,float(sval0.get()),float(sval1.get()))
                        ax.plot(a.t,E)

    if ft.get() == 'f':
        ax.set_xlabel('f/(GHz)')
        ax.set_ylabel('E/(arb. units)')
    if ft.get() == 't':
        ax.set_ylabel(r'$E/(\mu V m^{-1})$')
        ax.set_xlabel(r'$t/(ns)$')
    if ft.get() == 'g':
        ax.set_xlabel('f/(GHz)')
        ax.set_ylabel('group delay (ns)')

def _update_stations():
    global ax2
    station_plot(ax2)
    canvas2.draw()

def _update_station_list(event):
    global ax2
    station_plot(ax2)
    canvas2.draw()

def _next():
    global currentEventIndex
    if currentEventIndex + 1 >= len(fileNames):
        print('Last event already, so sad...')
    else:
        currentEventIndex += 1
        update_event()
        _update_array()
        _update_stations()

def _prev():
    global currentEventIndex
    if (currentEventIndex - 1) < 0:
        print('Already at first event')
    else:
        currentEventIndex = currentEventIndex - 1
        update_event()
        _update_array()
        _update_stations()

def print_event_info():
    global ev
    fn = fileNames[currentEventIndex]
    fn = fn[fn.rfind('/')+1:]
    s = 'filename: ' + fn + '\n'
    s += 'Primary: %s'%(ev.part_id) + '\n '
    s += '(zen,azi) = (%2.2f,%2.2f)'%(ev.zenith,ev.azimuth) + '\n'
    s += 'Energy %.2e EeV'%(ev.energy) + '\n'
    s += 'Xmax %2.2f g/cm2'%(ev.xmax) + '\n '
    return s




#BUTTONS TAB1
bframe = tk.Frame(master=tab1, borderwidth=1)
bframe.pack(fill=tk.BOTH, side=tk.LEFT, expand=True)
rx = ttk.Radiobutton(bframe, text="Ex ", variable=ecomp, value='Ex',command=_update_array)
ry = ttk.Radiobutton(bframe, text="Ey ", variable=ecomp, value='Ey',command=_update_array)
rz = ttk.Radiobutton(bframe, text="Ez ", variable=ecomp, value='Ez',command=_update_array)
ra = ttk.Radiobutton(bframe, text="|E|", variable=ecomp, value='Eabs',command=_update_array)
rx.grid(row=3,column=0,sticky='w')
ry.grid(row=4,column=0,sticky='w')
rz.grid(row=5,column=0,sticky='w')
ra.grid(row=6,column=0,sticky='w')
pa = ttk.Radiobutton(bframe, text="Max Ampl.", variable=plttype, value='maxampl',command=_update_array)
pt = ttk.Radiobutton(bframe, text="Max Time.", variable=plttype, value='time',command=_update_array)
ps = ttk.Radiobutton(bframe, text="sum(E)^2 ", variable=plttype, value='sumquad',command=_update_array)
pa.grid(row=3,column=1,sticky='w')
pt.grid(row=4,column=1,sticky='w')
ps.grid(row=5,column=1,sticky='w')
pa = ttk.Radiobutton(bframe, text="xyz   ", variable=coord, value='xyz',command=_update_array)
pt = ttk.Radiobutton(bframe, text="auger   ", variable=coord, value='auger',command=_update_array)
# pt = ttk.Radiobutton(bframe, text="shower", variable=coord, value='shower',command=_update_array)
ps = ttk.Radiobutton(bframe, text="-vxB  ", variable=coord, value='-vxB',command=_update_array)
pa.grid(row=7,column=1,sticky='w')
pt.grid(row=8,column=1,sticky='w')
ps.grid(row=9,column=1,sticky='w')
wid = tk.Checkbutton(bframe, text="log10 ", variable=useLog,command=_update_array)
wid.grid(row=10,column=0,sticky='w')
wid = tk.Checkbutton(bframe, text="Labels ", variable=textLabel,command=_update_array)
wid.grid(row=10,column=1,sticky='w')
text_box = tk.Text(bframe,height=7,width=50)
text_box.grid(row=11,column=0,columnspan=2,sticky='w')

#BUTTONS TAB2
bframe = tk.Frame(master=tab2, borderwidth=1)
bframe.pack(fill=tk.BOTH, side=tk.LEFT, expand=True)
listbox = tk.Listbox(bframe,listvariable=alabels,height=6,selectmode='extended')
listbox.grid(row = 0, column=0,sticky='nwes')
# link a scrollbar to a list
scrollbar = ttk.Scrollbar(bframe,orient='vertical',command=listbox.yview)
listbox['yscrollcommand'] = scrollbar.set
scrollbar.grid(column=0,row=0,sticky='ens')
listbox.bind('<<ListboxSelect>>', _update_station_list)
wid = tk.Checkbutton(bframe, text="Ex /AxB", variable=stx,command=_update_stations)
wid.grid(row=10,column=0,sticky='w')
wid = tk.Checkbutton(bframe, text="Ey/AxAxB", variable=sty,command=_update_stations)
wid.grid(row=11,column=0,sticky='w')
wid = tk.Checkbutton(bframe, text="Ez/A", variable=stz,command=_update_stations)
wid.grid(row=12,column=0,sticky='w')
pa = ttk.Radiobutton(bframe, text="time   ", variable=ft, value='t',command=_update_stations)
pt = ttk.Radiobutton(bframe, text="freq   ", variable=ft, value='f',command=_update_stations)
pg = ttk.Radiobutton(bframe, text="grp. delay", variable=ft, value='g',command=_update_stations)
pa.grid(row=7,column=1,sticky='w')
pt.grid(row=8,column=1,sticky='w')
pg.grid(row=9,column=1,sticky='w')
wid = tk.Checkbutton(bframe, text="log10-x", variable=useLogx,command=_update_stations)
wid.grid(row=10,column=1,sticky='w')
wid = tk.Checkbutton(bframe, text="log10-y", variable=useLogy,command=_update_stations)
wid.grid(row=11,column=1,sticky='w')
wid = tk.Checkbutton(bframe, text="AxB", variable=useAxB,command=_update_stations)
wid.grid(row=7,column=0,sticky='w')


#BUTTONS GENERAL FRAME
wid = ttk.Label(master=gframe, text="f0 (GHz)")
wid.grid(row=0,column=0,sticky='w')
values=tuple(np.linspace(0,freqMax,int(freqMax/0.005)+1))
wid = ttk.Spinbox(gframe, from_=0.0, to=freqMax, width=5, values=values,textvariable=sval0)
wid.grid(row=0,column=1,sticky='w')
wid = ttk.Label(master=gframe, text="f1 (GHz)")
wid.grid(row=1,column=0,sticky='w')
wid = ttk.Spinbox(gframe, from_=0.0, to=freqMax, width=5, values=values,textvariable=sval1)
wid.grid(row=1,column=1,sticky='w')
wid = ttk.Button(master=gframe, text="Previous", command=_prev)
wid.grid(row=1,column=3,sticky='w')
wid = ttk.Button(master=gframe, text="Next", command=_next)
wid.grid(row=1,column=4,sticky='w')
wid = ttk.Button(master=gframe, text="Quit", command=_quit)
wid.grid(row=1,column=5,sticky='w')
#initial function calls
update_event()
array_plot(ax1)
station_plot(ax2)

tk.mainloop()
