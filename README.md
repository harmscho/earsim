# earsim: Extensive Airshower Radio Simulation package

Some tools to deal with radio simulations.

Currently it can read in CoREAS and ZHAireS files (HDF5 format / `*sry + timefresnel-root.dat`  ASCII format) into a common event class. Examples of the interface are given the test folder (`test/read_coreas.py` / `test/read_zhaires.py`). In addition, there is an event viewer (`test/earsim_view.py`), which can be used to inspect multiple simulated events.
For CoREAS files
```
python test/earsim_view.py /path/where/i/store/my/sims/*/*.reas
```
or for ZHAireS 
```
python test/earsim_view.py /path/where/i/store/my/sims/*/*.sry
```

To install:
`pip install -e .`

needs: `numpy` `h5py` `matplotlib` `tk` (I usually install this with pip inside a conda enviroment)
