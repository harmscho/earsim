WP=$PWD
for i; do
    folder=${i%'/'*}
    cd $folder
    echo -e "#!/bin/bash \nZHAireS < *inp" > run.sh
    chmod a+x run.sh
    sbatch --partition=hef --time=4-0:00 run.sh
    cd $WP
done
