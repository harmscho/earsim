WP=$PWD
for i; do
    folder=${i%'/'*}
    cd $folder
    files=*.inp
    RESWD=$PWD
    for f in $files
    do
	echo -e "#!/bin/bash \nexport PATH=$PATH;\ncp $f \$TMPDIR;cd \$TMPDIR;ZHAireS < $f|grep -v \"Effective \";mv * $RESWD" > run.sh
	#echo -e "#!/bin/bash \nexport PATH=$PATH; ZHAireS < $f|grep -v \"Effective \"" > run.sh 
	cat run.sh
	chmod a+x run.sh
	qsub -j oe -q generic -o $RESWD/run.out -V -d $RESWD run.sh
    done
    cd $WP
done
