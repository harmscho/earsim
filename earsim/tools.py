import numpy as np
from scipy.signal import savgol_filter
def get_uvw(zen,azi):
    ''''unit vectors u,v,w from axis "a" with zenith and azimuth angle
    w, in direction on the sky.
    u, projection of "a" in xy plane, but 90 degrees with w
    v, cross product w x u
    in the case zen = azi = 0, u = x, v= y, w = z
    '''
    zen = np.deg2rad(zen)
    azi = np.deg2rad(azi)
    w = [np.cos(azi)*np.sin(zen),np.sin(azi)*np.sin(zen),np.cos(zen)]
    u = [np.cos(azi)*np.cos(zen),np.sin(azi)*np.cos(zen),-np.sin(zen)]
    v = np.cross(w,u)
    return  u,v,w

def get_freq_spec(val,dt):
    fval = np.abs(np.fft.fft(val))[:len(val)//2]
    freq =  np.fft.fftfreq(len(val),dt)[:len(val)//2]
    return freq,fval

def get_group_delay(val,dt,smooth=0):
    fval = np.angle(np.fft.fft(val))[:len(val)//2]
    freq =  np.fft.fftfreq(len(val),dt)[:len(val)//2]
    df = freq[1]-freq[0]
    #derivative
    dx = fval[1:] - fval[0:-1]
    # to avoid 2pi jumps
    dx = np.arccos(np.cos(dx))
    dt = dx/(df*2*np.pi)
    if smooth != 0:
        if smooth%2!=1:
            smooth = smooth + 1
        dt = savgol_filter(dt, smooth, 3)
    freq = (freq[1:] + freq[0:-1])/2
    #
    # dt = dt - dt[np.argmax(freq>0.05)]
    return freq,dt

def block_filter(val,dt,f0,f1):
    fval = np.fft.fft(val)
    freq =  np.fft.fftfreq(len(val),dt)
    for i,f in enumerate(freq):
        if not(f0 <= np.abs(f) <= f1):
            fval[i] = 0.+0j
    fval = np.real(np.fft.ifft(fval))
    return np.asarray(fval)
