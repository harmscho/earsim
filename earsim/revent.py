import numpy as np
import h5py
import glob
class Antenna:
    """ antenna with location and timeseries of Efield"""
    def __init__(self,x,y,z,name=""):
        self.x = x
        self.y = y
        self.z = z
        self.loc=np.array([x,y,z])
        self.name = name

class REvent:
    """ An interface class for reading simulations """
    def __init__(self,filename,parse_antenna=True):
        self.mergemode = False
        self.reasfile = filename
        self.antennas = []
        self.core = [0,0,0]
        if '.reas' in filename:
            self.reasfile = filename
            self.parse_reas(parse_antenna)
        elif '.hdf5' in filename:
            self.hdf5file = filename
            self.parse_hdf5()
        elif '.sry' in filename:
            self.sryfile = filename
            self.parse_sry(parse_antenna)
        else:
            print('no file type given, looking for folders starting with sub')
            if filename[-1] != '/':
                filename = filename + '/'
            lsry = glob.glob(filename + "sub**/*.sry",recursive=True)
            lreas = glob.glob(filename + "sub**/*.reas",recursive=True)
            if len(lsry) != 0:
                print('reading in merge mode',lsry)
                self.mergemode = True
                for l in lsry:
                    print('parsing',l)
                    self.sryfile = l
                    self.parse_sry()
            if len(lreas) != 0:
                print('reading in merge mode',lreas)
                self.mergemode = True
                for l in lreas:
                    print('parsing',l)
                    self.reasfile = l
                    self.parse_reas(parse_antenna)
        self.set_unit_vectors()

    def parse_sry(self,parse_antenna):
        """ parse *sry file """
        print('reading:',self.sryfile)
        f = open(self.sryfile)
        lines = f.readlines()
        inParticleGround=False
        for l in lines:
            if 'Primary zenith angle:' in l:
                self.zenith = float(l.split()[3])
            if 'Primary azimuth angle:' in l:
                self.azimuth = float(l.split()[3])
                if self.azimuth > 360:
                    self.azimuth = self.azimuth - 360
            if 'Sl. depth of max. (g/cm2):' in l:
                self.xmax = float(l.split()[5])
            if 'Primary energy:' in l and len(l.split())== 4:
                self.energy = float(l.split()[2])
                unit = l.split()[3]
                if unit == "EeV":
                    self.energy = self.energy * 1
                elif unit == "PeV":
                    self.energy = self.energy * 1e-3
                elif unit == "TeV":
                    self.energy = self.energy * 1e-6
                elif unit == "GeV":
                    self.energy = self.energy * 1e-9
                else:
                    print("unknown unit for energy ",unit)
                    print(l)
            if 'Charged pcles. at maximum:' in l:
                self.chprt = float(l.split()[4])*1e6
            if 'Primary particle:' in l:
                self.part_id = str(l.split()[2])
            if 'Injection altitude: ' in l:
                off = 0
                if l.split()[0] == '(D)':
                    off = 1
                self.InjAlt = float(l.split()[2+off])*1e3#Km to meters
            if 'I:' in l:
                off = 0
                if l.split()[0] == '(D)':
                    off = 1
                self.Binc = float(l.split()[1+off])
            if 'Geomagnetic field: Intensity:' in l:
                off = 0
                if l.split()[0] == '(D)':
                    off = 1
                self.Bmag = float(l.split()[3+off])
            if "Sl. first interact. depth:" in l:
                self.X0 = float(l.split()[4])
            if "Ground altitude:" in l:
                self.zgr = float(l.split()[2]) * 1e3
            if '>>>>' in l:
                if 'PARTICLES REACHING GROUND LEVEL' in l:
                    inParticleGround = True
                    scaling = 1.
                else:
                    inParticleGround = False
            if inParticleGround:
                if "units of 10^3 particles" in l:
                    scaling = 1e3
                if "units of 10^6 particles" in l:
                    scaling = 1e6
                if 'All muons' in l:
                    self.Nmu_gr = float(l.split()[2])*scaling
        print("parsed sry file")
        if parse_antenna:
            self.parse_sry_antenas()
        self.parse_tables()
    
    def parse_sry_antenas(self):
        """Parse the antennas by sry """
        i = self.sryfile.rfind(r'/')
        path = ''
        if i == -1:
            i = self.sryfile.rfind(r'\\')
        if i != -1:
            path = self.sryfile[0:i+1]
        fsnelFile = glob.glob(path + '*snel-root.dat')
        print("reading waveforms from ",fsnelFile)
        if len(fsnelFile) != 1:
            print("WARNING")
        else:
            print("fsnelFile:",fsnelFile[0])
        self.posfile = fsnelFile[0]
        data=np.loadtxt(self.posfile)
        aii=[]
        if np.shape(data)[1] == 5:
            i_t = 1
            i_Ex = 2
            i_Ey = 3
            i_Ez = 4
            i_id = 0
            for i in range(np.shape(data)[0]):
                aii =np.append(aii,data[i][0])
                aii =np.unique(aii)
        elif np.shape(data)[1] == 14:
            i_t = 5
            i_Ex = 11
            i_Ey = 12
            i_Ez = 13
            i_id = 1
            for i in range(np.shape(data)[0]):
                aii =np.append(aii,data[i][1])
                aii =np.unique(aii)
        else: 
            print('unexpected shape of timeseries file')    
        
        start_index=len(self.antennas)
        print('reading ' +  str(len(aii)) + ' antennas')
        ax=0                
        self.get_antenna_info()        
        for a in self.antennas[start_index:]:            
            a.t = []
            a.Ex =[]
            a.Ey =[]
            a.Ez =[]
            a.t=data[data[:,i_id]==aii[ax]][:,i_t]
            a.Ex=data[data[:,i_id]==aii[ax]][:,i_Ex]
            a.Ey=data[data[:,i_id]==aii[ax]][:,i_Ey]
            a.Ez=data[data[:,i_id]==aii[ax]][:,i_Ez]
            ax+=1
            a.t = np.asarray(a.t)
            a.Ex = np.asarray(a.Ex)*1e6#microV
            a.Ey = np.asarray(a.Ey)*1e6#microV
            a.Ez = np.asarray(a.Ez)*1e6#microv
            a.Ez = -a.Ez
        print("done reading antennas")

    def get_antenna_info(self):
        """ get antenna info from sry file """
        fp = open(self.sryfile)
        lines = fp.readlines()        
        for il, l in enumerate(lines):            
            if "Antennas used for" in l:                
                iil = il+3                
                while "Time bin size" not in lines[iil] and len(lines[iil]) > 5:                    
                    data = lines[iil].rstrip().split()                    
                    iil = iil + 1
                    if len(data) == 5:
                        a = Antenna(float(data[1]),float(data[2]),float(data[3])-self.zgr,int(data[0]))
                        self.antennas.append(a)
                    elif len(data) == 6:
                        a = Antenna(float(data[2]),float(data[3]),float(data[4])-self.zgr,int(data[0]))
                        a.label = data[1]
                        self.antennas.append(a)
                    else :
                        print("unexpected antenna info")
                break
        fp.close()
        return None
      
    def parse_tables(self):
        """
        Reading longitudinal development table.
        To get slant depth tables add to the input card:
            ExportTable 1291 Opt a
            ExportPerShower
        """
        print("parse tables")
        self.Xgrammage=[]
        self.Nchpart=[]
        i = self.sryfile.rfind(r'/')
        path = ''
        if i == -1:
            i = self.sryfile.rfind(r'\\')
        if i != -1:
            path = self.sryfile[0:i+1]
        tabFile = glob.glob(path + '*.t1291')
        if len(tabFile) != 1:
            print("no tables to be parsed")
            return False
        self.posfile = tabFile[0]
        data2=np.loadtxt(self.posfile,usecols=(1,2))
        self.Xgrammage=data2[:,0]
        self.Nchpart=data2[:,1]
        return True

    def parse_reas(self,parse_antenna):
        """ parse *reas file """
        f = open(self.reasfile)
        lines = f.readlines()
        self.core = [0,0,0]
        self.MagneticAzimuth = 0
        for l in lines:
            if 'ShowerZenithAngle' in l:
                self.zenith = float(l.split()[2])
            if 'ShowerAzimuthAngle' in l:
                self.azimuth = float(l.split()[2]) + 180
                if self.azimuth > 360:
                    self.azimuth = self.azimuth - 360
            if 'DepthOfShowerMaximum' in l:
                self.xmax = float(l.split()[2])
            if 'PrimaryParticleEnergy' in l:
                self.energy = float(l.split()[2])/1e18
            if 'PrimaryParticleType' in l:
                self.part_id = int(l.split()[2])
            if 'MagneticFieldInclinationAngle' in l:
                self.Binc = float(l.split()[2])
            if 'MagneticFieldStrength' in l:
                self.Bmag = float(l.split()[2])
            if 'DistanceOfShowerMaximum' in l:
                self.dist_xmax = float(l.split()[2])/100 #m
            if 'CoreCoordinateVertical' in l:
                self.core = [0,0,float(l.split()[2])/100]
            if 'RotationAngleForMagfieldDeclination' in l:
                self.MagneticAzimuth = float(l.split()[2])
            if 'CoreCoordinateVertical' in l:
                self.zgr = float(l.split()[2])/100
        # self.azimuth = self.azimuth - self.MagneticAzimuth
        if parse_antenna == True:
            self.parse_reas_antenas()
        self.core = [0,0,0]

    def parse_reas_antenas(self):
        """Parse the antennas by reas """
        self.listfile = self.reasfile.replace('.reas','.list')
        f = open(self.listfile)
        lines = f.readlines()
        print('reading ' +  str(len(lines)) + ' antennas')
        for l in lines:
            x = float(l.split()[2])/100
            y = float(l.split()[3])/100
            z = float(l.split()[4])/100
            x = x - self.core[0]
            y = y - self.core[1]
            z = z - self.core[2]
            a = Antenna(x,y,z,l.split()[5])
            efile = self.listfile.replace('.list','_coreas/raw_'+ a.name + '.dat')
            a.t, a.Ex, a.Ey, a.Ez = np.loadtxt(efile,unpack=True)
            a.t = np.asarray(a.t)*1e9 #use nanoseconds
            a.Ex = np.asarray(a.Ex) * 2.99792458e10 #cgs statvolt/cm volt -> mu V/m (SI)
            a.Ey = np.asarray(a.Ey) * 2.99792458e10 #cgs statvolt/cm volt-> mu V/m (SI)
            a.Ez = np.asarray(a.Ez) * 2.99792458e10 #cgs statvolt/cm volt-> mu V/m (SI)
            self.antennas.append(a)

    def parse_hdf5(self):
        print('parsing hdf5 file')
        f = h5py.File(self.hdf5file, "r")
        print('opened for reading')
        self.antennas = []
        print(list(f.keys()))
        ai = f[list(f.keys())[-1] + '/AntennaInfo']
        
        for ai_ in ai:
            a = Antenna(ai_[1],ai_[2],ai_[3],ai_[0].decode('UTF-8'))
            self.antennas.append(a)
        print('reading ' +  str(len(self.antennas)) + ' antennas')

        traces = f[list(f.keys())[-1] + '/AntennaTraces']
        for tr in traces:
            for a in self.antennas:
                if a.name == tr:
                    break
            a.t = []
            a.Ex = []
            a.Ey = []
            a.Ez =[]
            for tup in traces[tr + '/efield']:
                a.t.append(tup[0])
                a.Ex.append(tup[1])
                a.Ey.append(tup[2])
                a.Ez.append(tup[3])
            a.t = np.asarray(a.t)
            a.Ex = np.asarray(a.Ex)
            a.Ey = np.asarray(a.Ey)
            a.Ez = np.asarray(a.Ez)

        ei = f[list(f.keys())[-1] + '/EventInfo']
        ei=ei[0]
        self.zenith = 180.-ei[4]
        self.azimuth = ei[5]+180
        if self.azimuth >= 360:
            self.azimuth -= 360
        self.xmax = ei[9]
        self.energy = ei[3]
        self.part_id = ei[2].decode('UTF-8')
        self.Bmag = ei[16]
        self.Binc = ei[17]
        self.Bdec = ei[18]
        self.ground=ei[11]
        self.dist_xmax=ei[6]

    def set_unit_vectors(self):
        self.Bx = self.Bmag * np.cos(np.deg2rad(self.Binc))
        self.By = 0
        self.Bz = -self.Bmag * np.sin(np.deg2rad(self.Binc))
        self.uB = np.asarray([self.Bx/self.Bmag,self.By/self.Bmag,self.Bz/self.Bmag])
        ux = np.sin(np.deg2rad(self.zenith)) * np.cos(np.deg2rad(self.azimuth))
        uy = np.sin(np.deg2rad(self.zenith)) * np.sin(np.deg2rad(self.azimuth))
        uz = np.cos(np.deg2rad(self.zenith))
        self.uA = np.asarray([ux,uy,uz])
        self.uAxB = np.cross(self.uA,self.uB)
        self.uAxB = self.uAxB/(np.dot(self.uAxB,self.uAxB))**0.5
        self.uAxAxB = np.cross(self.uA,self.uAxB)
